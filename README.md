Комманды:

- Run `gulp serve` to preview and watch for changes
- Run `bower install --save <package>` to install frontend dependencies
- Run `gulp serve:test` to run the tests in the browser
- Run `gulp` to build your webapp for production
- Run `gulp serve:dist` to preview the production build

Сборка в папке `dist`

Больше комманд в документации [Web app generator](https://github.com/yeoman/generator-webapp)

