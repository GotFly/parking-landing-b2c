'use strict';
var _extends = Object.assign || function(e) {
  for (var t = 1; t < arguments.length; t++) {
    var o = arguments[t];
    for (var a in o) Object.prototype.hasOwnProperty.call(o, a) && (e[a] = o[a])
  }
  return e
};
$(document).ready(function() {
  $('.modal').on('hidden.bs.modal', function (e) {
    $('.modal form').get(0).reset();
    $('div.form-group').removeClass('has-danger');
  });
  $('.form-control').keydown(function (e) {
    if (e.which === 13) {
      var index = $('.form-control').index(this) + 1;
      $('.form-control').eq(index).focus();
    }
  });
});


$(document).ready(function() {
  $('.page-down').on('click', function(e) {
    e.preventDefault();
    var t = $(this).attr('data-href'),
      o = $(t).offset().top;
    $('body,html').animate({
      scrollTop: o
    }, 1e3)
  })
  $('.rewiews-slider').slick({
    slidesToShow: 1,
    appendArrows: $('.arrows'),
    responsive: [{
      breakpoint: 1024,
      settings: {
        centerMode: !1,
        centerPadding: '0'
      }
    }, {
      breakpoint: 768,
      settings: {
        centerMode: !0,
        centerPadding: '17px'
      }
    }]
  })
}),'use strict';
! function() {
    const REDIRECT_URL = 'https://lk.parkomatica.ru';

    function mapCodeToField(code) {
        switch (code) {
            case 1310:
                return 'phone';
            case 2204:
            case 2211:
                return 'promocode';
        }
    }

    function sendRequest($form, params = {}) {
        var $submit = $form.find('[type="submit"]'),
            url = $form.prop('action'),
            method = $form.data('method') || $form.prop('method') || 'GET',
            data = {};

        $form.serializeArray().map(item => {
            if (item.name) {
                data[item.name] = item.value;
            }
        });

        if (data.phone) {
            data.phone = data.phone.replace(/\D/g,'');
        }

        $form.addClass('default-form--loading');
        $submit.prop('disabled', true);

        return $.ajax(Object.assign({
            url,
            method,
            data,
        }, params))
            .always(() => {
                $form.removeClass('default-form--loading');
            });
    }

    function showFormMessage($form, type, content) {
        $form.addClass('default-form--show-message');
        /*setTimeout(() => {
          $form[0].reset();
          $form.removeClass('default-form--show-message');
        }, 6000);*/
    }

  var e = 'https://lk.parkomatica.ru',
    o = Cookies.get('login');
  if (window.location.search.match('test')) {
    var a = document.createElement('script');
    a.src = '//widget.happydesk.ru/widget.js', document.body.appendChild(a), a.onload = function() {
      Happydesk.initChat({
        clientId: 209,
        enableChat: !0
      }, {
        page_url: window.location.href,
        user_agent: window.navigator.userAgent,
        language: 'ru'
      })
    }
  }
  $(function() {
    function a(e) {
      var o = e.val(),
        a = !0;
      if ('promocode' === e.attr('name')) return !0;
      if (o && o.trim() || (a = !1), 'name' === e.attr('name')) {
        a = /^[А-ЯЁA-Z-а-яёa-z]|[А-ЯЁA-Z-а-яёa-z]{1,19}$/.test(o)
      }
      if (o && o.trim() || (a = !1), 'email' === e.attr('name')) {
        a = /^[a-zA-Z0-9\-._]{1,}@[(mail.ru|bk.ru|inbox.ru|list.ru|yandex.ru|gmail.com|yahoo.(ru|com)|hostmail.(ru|com)|outlook.(ru|com))]{5,11}$/.test(o)
        //a = /^[\w\.\d-_]+@[\w\.\d-_]+\.\w{2,4}$/.test(o)
      }
      if ('phone' === e.attr('name') && (a = o && -1 === o.indexOf('_')), 'pin' === e.attr('name')) {
        a = /^[0-9]{5}$/.test(o)
      }
      return a
    }

    function t(e) {
      var o = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
        a = e.find('[type="submit"]'),
        t = e.prop('action'),
        n = e.data('method') || e.prop('method') || 'GET',
        r = {};
      return e.serializeArray().map(function(e) {
        e.name && (r[e.name] = e.value)
      }), r.phone && (r.phone = r.phone.replace(/\D/g, '')), e.addClass('default-form--loading'), a.prop('disabled', !0), $.ajax(Object.assign({
        url: t,
        method: n,
        data: r
      }, o)).always(function() {
        e.removeClass('default-form--loading'), a.prop('disabled', !1)
      })
    }

    function n(e, o, a) {
      e.addClass('default-form--show-message'), setTimeout(function() {
        e[0].reset(), e.removeClass('default-form--show-message')
      }, 6e3)
    }

    function r() {
      $('.js-block-login').addClass('is-pin-shonw')
    }

    function s() {
      $('.js-block-login').removeClass('is-pin-shonw')
    }

    function i(e) {
      switch (e) {
        case 1310:
          return 'phone';
        case 2204:
        case 2211:
          return 'promocode'
      }
    }
    switch ($('.js-phone').mask('+7 (999) 999-99-99'), $('.js-pin').mask('99999', {
      placeholder: '*'
    }), $('[data-scroll-to]').click(function(e) {
      e.preventDefault();
      var o = e.target.getAttribute('href').slice(1),
        a = document.getElementsByName(o)[0].getBoundingClientRect().top;
      $('html, body').animate({
        scrollTop: a
      }, 500)
    }), $('[data-login]').click(function(a) {
      a.preventDefault(), o ? window.location = e : $('#loginModal').modal('show')
    }), window.location.hash) {
      case '#login':
        $('[data-login]').eq(0).click(), window.location.hash = '';
        break;
      case '#orderModal':
        $('#orderModal').modal('show'), window.location.hash = ''
    }
    $('.default-form').on('keyup input', function(e) {
      var o = $(e.target),
        t = $(e.currentTarget),
        n = t.find('[type="submit"]'),
        r = t.find('[name]');
      o.val() && o.data('dirty', !0);
      var s = !0;
      r.each(function(e, o) {
        var t = $(o);
        a(t) ? t.closest('.form-group').removeClass('has-danger').find('.form-control-feedback').html('') : (t.data('dirty') && t.closest('.form-group').addClass('has-danger'), s = !1)
      }), n.prop('disabled', !s)
    }), $('.js-default-form').on('submit', function(e) {
      e.preventDefault();
      var o = $(e.target);
      t(o).always(function(e) {
        n(o)
      })
    }),
      $('#order-form').on('submit', event => {
        event.preventDefault();

        const $form = $(event.target),
          jqXHR = sendRequest($form, {
            dataType: 'json'
          });

        jqXHR.done(response => {
            if (response.status === 'ok') {
                //showFormMessage($form);

                if (typeof yaCounter46630227 !== 'undefined') {
                    yaCounter46630227.reachGoal('FORM_ORDER_SUBMIT');
                }
                ga('send', 'event', 'form', 'success', 'order parkomatca');

                $form.find(':input, button').prop('disabled', true);
                window.location = REDIRECT_URL;
            } else {
                if (response.status === 'error') {
                    if (response.error && response.error.code) {
                      console.log($form);
                        const $errorInput = $form.find(`[name="${mapCodeToField(response.error.code)}"]`);
                        console.log($errorInput);
                        const message = (response.error.code === 1310) ? `Такой телефон уже зарегистрирован. ${$('#loginModal').length ? `
                            Попробуйте <a class="js-login-from-order-link"
                            href = "javascript:void(0);"
                            data-phone="${$errorInput.val()}">войти</a>  в личный кабинет` : ''}`: response.error.message;

                        $errorInput
                            .closest('.form-group')
                            .addClass('has-danger')
                            .find('.form-control-feedback')
                            .html(message);
                    }
                }
            }
        }).fail(response => {
          console.error(`Ошибка ${response.status}`);
        });
      }),
      $('#order-form').on('click', '.js-login-from-order-link', function(e) {
        var o = $(e.target),
          a = $('#loginModal');
        e.preventDefault(), a.find('[name="phone"]').val(o.data('phone')), $('#orderModal').modal('hide'), a.modal('show')
      }),
      $('#login-form').on('submit', function(o) {
        o.preventDefault();
        var a = $(o.target),
          n = t(a, {
            dataType: 'text'
          }),
          s = a.find('[name="phone"]');
        n.done(function(o) {
          if (o) {
            if (o = JSON.parse(o), 'error' == o.status) {
              o.error && 1001 == o.error.code && (window.location = e);
              var a = o.error && o.error.message ? o.error.message : 'Что-то пошло не так';
              s.closest('.form-group').addClass('has-danger').find('.form-control-feedback').html(a)
            }
          } else {
            var t = s.val().replace(/\D/g, '');
            $('#pin-form').find('[name="phone"]').val(t), r()
          }
        })
      }), $('#pin-form').on('submit', function(o) {
      o.preventDefault();
      var a = $(o.target),
        n = t(a, {
          dataType: 'text'
        }),
        r = a.find('[name="pin"]');
      n.done(function(o) {
        if (o) {
          if (o = JSON.parse(o), 'error' == o.status) {
            var a = o.error && o.error.message ? o.error.message : 'Что-то пошло не так';
            o.error && 1001 == o.error.code && (window.location = e), r.closest('.form-group').addClass('has-danger').find('.form-control-feedback').html(a)
          }
        } else s(), window.location = e, $('#loginModal').modal('hide')
      })
    })
  })
}()
