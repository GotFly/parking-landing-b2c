'use strict';

(function () {
  $(function () {
    const $animatedBlocks = $('.animate');

    let windowHeight = $(window).height();

    checkBlockVisability();

    // show content on scroll;
    $(window).on('scroll', checkBlockVisability);

    $(window).on('resize', event => {
      windowHeight = $(window).height();
      checkBlockVisability();
    });

    function checkBlockVisability() {
      const scroll = $(window).scrollTop();

      $animatedBlocks.each((index, item) => {
        const $block = $(item),
          blockOffsetTop = $block.offset().top;

        if (scroll + windowHeight*0.7 > blockOffsetTop) {
          $block.addClass('show-content');
        } else {
          $block.removeClass('show-content');
        }
      });
    }


    // parallax
    if ($('.promo-parallax-container').length) {
      $('.promo-parallax__layer').plaxify();
      $.plax.enable();

      // check car position
      $(window).on('scroll', function() {
          const windowScroll = $(window).scrollTop();

          if (windowScroll > windowHeight*0.7) {
            $('#promo-parallax-moved-car').addClass('move-parallax-car');
          } else {
            $('#promo-parallax-moved-car').removeClass('move-parallax-car');
          }
      });
    }

  });
})();