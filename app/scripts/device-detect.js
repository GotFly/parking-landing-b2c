(function() {
  var _addClass, _doc_element, _find, _handleOrientation, _hasClass, _orientation_event, _removeClass, _supports_orientation, _user_agent;

  window.deviceDetect = {};

  _doc_element = window.document.documentElement;

  _user_agent = window.navigator.userAgent.toLowerCase();

  deviceDetect.ios = function() {
    return deviceDetect.iphone() || deviceDetect.ipod() || deviceDetect.ipad();
  };

  deviceDetect.iphone = function() {
    return _find('iphone');
  };

  deviceDetect.ipod = function() {
    return _find('ipod');
  };

  deviceDetect.ipad = function() {
    return _find('ipad');
  };

  deviceDetect.android = function() {
    return _find('android');
  };

  deviceDetect.androidPhone = function() {
    return deviceDetect.android() && _find('mobile');
  };

  deviceDetect.androidTablet = function() {
    return deviceDetect.android() && !_find('mobile');
  };

  deviceDetect.blackberry = function() {
    return _find('blackberry') || _find('bb10') || _find('rim');
  };

  deviceDetect.blackberryPhone = function() {
    return deviceDetect.blackberry() && !_find('tablet');
  };

  deviceDetect.blackberryTablet = function() {
    return deviceDetect.blackberry() && _find('tablet');
  };

  deviceDetect.windows = function() {
    return _find('windows');
  };

  deviceDetect.windowsPhone = function() {
    return deviceDetect.windows() && _find('phone');
  };

  deviceDetect.windowsTablet = function() {
    return deviceDetect.windows() && _find('touch');
  };

  deviceDetect.fxos = function() {
    return _find('(mobile; rv:') || _find('(tablet; rv:');
  };

  deviceDetect.fxosPhone = function() {
    return deviceDetect.fxos() && _find('mobile');
  };

  deviceDetect.fxosTablet = function() {
    return deviceDetect.fxos() && _find('tablet');
  };

  deviceDetect.mobile = function() {
    return deviceDetect.androidPhone() || deviceDetect.iphone() || deviceDetect.ipod() || deviceDetect.windowsPhone() || deviceDetect.blackberryPhone() || deviceDetect.fxosPhone();
  };

  deviceDetect.tablet = function() {
    return deviceDetect.ipad() || deviceDetect.androidTablet() || deviceDetect.blackberryTablet() || deviceDetect.windowsTablet() || deviceDetect.fxosTablet();
  };

  deviceDetect.portrait = function() {
    return Math.abs(window.orientation) !== 90;
  };

  deviceDetect.landscape = function() {
    return Math.abs(window.orientation) === 90;
  };

  _find = function(needle) {
    return _user_agent.indexOf(needle) !== -1;
  };

  _hasClass = function(class_name) {
    var regex;
    regex = new RegExp(class_name, 'i');
    return _doc_element.className.match(regex);
  };

  _addClass = function(class_name) {
    if (!_hasClass(class_name)) {
      return _doc_element.className += ' ' + class_name;
    }
  };

  _removeClass = function(class_name) {
    if (_hasClass(class_name)) {
      return _doc_element.className = _doc_element.className.replace(class_name, '');
    }
  };

  if (deviceDetect.ios()) {
    if (deviceDetect.ipad()) {
      _addClass('ios ipad tablet');
    } else if (deviceDetect.iphone()) {
      _addClass('ios iphone mobile');
    } else if (deviceDetect.ipod()) {
      _addClass('ios ipod mobile');
    }
  } else if (deviceDetect.android()) {
    if (deviceDetect.androidTablet()) {
      _addClass('android tablet');
    } else {
      _addClass('android mobile');
    }
  } else if (deviceDetect.blackberry()) {
    if (deviceDetect.blackberryTablet()) {
      _addClass('blackberry tablet');
    } else {
      _addClass('blackberry mobile');
    }
  } else if (deviceDetect.windows()) {
    if (deviceDetect.windowsTablet()) {
      _addClass('windows tablet');
    } else if (deviceDetect.windowsPhone()) {
      _addClass('windows mobile');
    } else {
      _addClass('desktop');
    }
  } else if (deviceDetect.fxos()) {
    if (deviceDetect.fxosTablet()) {
      _addClass('fxos tablet');
    } else {
      _addClass('fxos mobile');
    }
  } else {
    _addClass('desktop');
  }

  _handleOrientation = function() {
    if (deviceDetect.landscape()) {
      _removeClass('portrait');
      return _addClass('landscape');
    } else {
      _removeClass('landscape');
      return _addClass('portrait');
    }
  };

  _supports_orientation = 'onorientationchange' in window;

  _orientation_event = _supports_orientation ? 'orientationchange' : 'resize';

  if (window.addEventListener) {
    window.addEventListener(_orientation_event, _handleOrientation, false);
  } else if (window.attachEvent) {
    window.attachEvent(_orientation_event, _handleOrientation);
  } else {
    window[_orientation_event] = _handleOrientation;
  }

  _handleOrientation();

}).call(this);
